from tkinter import *
from maze import *

### PARAMETRE DU LABYRINTHE :
x   = 5 # nbr de lignes dans le labyrinthe
y   = 5 # nbr de colonnes dans le labyrinthe
eh  = 20 #ecart haut
eg  = 20 #ecart gauche
lc  = 25 #largeur cellule
lt  = 1 #largeur trait
cap = "projecting"
name_file = "test.eps"
############################


L = Maze(x,y,"K")


fen=Tk()
can=Canvas(fen, width=eg*2+x*lc, height=eh*2+y*lc, bg='ivory')
can.pack()

can.create_line(     eg,      eh, eg+x*lc,      eh, fill='black', width=lt, capstyle=cap)
can.create_line(     eg,      eh,      eg, eh+y*lc, fill='black', width=lt, capstyle=cap)
can.create_line(eg+x*lc, eh+y*lc, eg+x*lc,      eh, fill='black', width=lt, capstyle=cap)
can.create_line(     eg, eh+y*lc, eg+x*lc, eh+y*lc, fill='black', width=lt, capstyle=cap)

for i,j,door in L.verticalDoors():
    if door: can.create_line(eg+(j+1)*lc, eh+i*lc, eg+(j+1)*lc, eh+(i+1)*lc, fill='black', width=lt, capstyle=cap)
            
for i,j,door in L.horizontalDoors():
    if door: can.create_line(eg+j*lc, eh+(i+1)*lc, eg+(j+1)*lc, eh+(i+1)*lc, fill='black', width=lt, capstyle=cap)

can.update()
can.postscript(file="test.eps")
 
