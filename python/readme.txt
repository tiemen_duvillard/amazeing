
- readme.txt
    Ce fichier

- maze.py
    La librairie python permettant la gestion de labyrinthe

Des fichiers tests :
- basic_maze.py
    Affichage basic d'un labyrinthe en txt

- print_maze.py
    Affichage graphique (tkinter) d'un labyrinthe

- solve_maze.py
    Affichage graphique (tkinter) d'un labyrinthe
    avec le chemin reliant 2 coins opposés

- longestWay_maze.py
    Affichage graphique (tkinter) d'un labyrinthe
    avec le chemin reliant les 2 cases les plus
    éloignés

- save_maze.py
    Enregistrement et ouverture d'un labyrinthe
    dans un fichier txt

- convert_maze.py
    Conversion d'un labyrinthe "a portes"
    en labyrinthe à case (graphiquement)

- play_maze.py
    Utilisation du module pour crée un jeu 
    où le but est de résoudre le labyrinthe
    à travers le pions que l'on controle 
    avec les fleches

- playhelp_maze.py
    Pareil que play_maze.py, mais avec une
    aide en plus

- postscript_maze.py
    Ecriture d'une image vectoriel
