﻿
from tkinter import *
from random import randrange
from maze import *

### PARAMETRE DU LABYRINTHE :
x   = 50 # nbr de lignes dans le labyrinthe
y   = 50 # nbr de colonnes dans le labyrinthe
eh  = 10 #ecart haut
eg  = 10 #ecart gauche
lc  = 20 #largeur cellule
lt  = 1 #largeur trait
cap = "projecting"
###




L = Maze(x,y,"K")

fen=Tk()
can=Canvas(fen,width=eg*2+x*lc,height=eh*2+y*lc,bg='ivory')
can.pack()


start, path, end = L.longestWay()

j,i = start[0],start[1]
can.create_rectangle(eg+j*lc, eh+i*lc, eg+(j+1)*lc, eh+(i+1)*lc, fill="green", width=0)
for s in path:
    j,i = L.move(j,i,s)
    can.create_rectangle(eg+j*lc, eh+i*lc, eg+(j+1)*lc, eh+(i+1)*lc, fill="pink", width=0)
can.create_rectangle(eg+j*lc, eh+i*lc, eg+(j+1)*lc, eh+(i+1)*lc, fill="green", width=0)


can.create_line(     eg,      eh, eg+x*lc,      eh, fill='black', width=lt, capstyle=cap)
can.create_line(     eg,      eh,      eg, eh+y*lc, fill='black', width=lt, capstyle=cap)
can.create_line(eg+x*lc, eh+y*lc, eg+x*lc,      eh, fill='black', width=lt, capstyle=cap)
can.create_line(     eg, eh+y*lc, eg+x*lc, eh+y*lc, fill='black', width=lt, capstyle=cap)

for i,j,door in L.verticalDoors():
    if door: can.create_line(eg+(j+1)*lc, eh+i*lc, eg+(j+1)*lc, eh+(i+1)*lc, fill='black', width=lt, capstyle=cap)
            
for i,j,door in L.horizontalDoors():
    if door: can.create_line(eg+j*lc, eh+(i+1)*lc, eg+(j+1)*lc, eh+(i+1)*lc, fill='black', width=lt, capstyle=cap)

print("longuestWay        :",len(path)+1)
print(start,end)
print("proportion of maze :",100*(len(path)+1)/(x*y),"%")
fen.mainloop()
