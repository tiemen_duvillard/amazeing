﻿
from tkinter import *
from random import randrange
from maze import *

### PARAMETRE DU LABYRINTHE :
x   = 20 # nbr de lignes dans le labyrinthe
y   = 20 # nbr de colonnes dans le labyrinthe
eh  = 20 #ecart haut
eg  = 20 #ecart gauche
lc  = 10 #largeur cellule
lt  = 1 #largeur trait
cap = "projecting"
###

cg = randrange(4) # coin gagnant : 0 = haut gauche ; 1 = haut droite ; 2 = bas droite ; 3 = bas gauche
g = 0
if cg == 0:
    Xa,Ya = 0, 0
    Xp,Yp = x-1, y-1
elif cg == 1:
    Xa,Ya = x-1, 0
    Xp,Yp = 0, y-1
elif cg == 2:
    Xa,Ya = x-1, y-1
    Xp,Yp = 0, 0
elif cg == 3:
    Xa,Ya = 0, y-1
    Xp,Yp = x-1, 0

    



L = Maze(x,y,"rc")

fen=Tk()
can=Canvas(fen,width=eg*2+x*lc,height=eh*2+y*lc,bg='ivory')
can.pack()


S = L.solve(Xa,Ya,Xp,Yp)
j,i = Xa, Ya
can.create_rectangle(eg+j*lc, eh+i*lc, eg+(j+1)*lc, eh+(i+1)*lc, fill="lightgreen", width=0)
for s in S:
    j,i = L.move(j,i,s)
    can.create_rectangle(eg+j*lc, eh+i*lc, eg+(j+1)*lc, eh+(i+1)*lc, fill="pink", width=0)
can.create_rectangle(eg+j*lc, eh+i*lc, eg+(j+1)*lc, eh+(i+1)*lc, fill="lightgreen", width=0)


can.create_line(     eg,      eh, eg+x*lc,      eh, fill='black', width=lt, capstyle=cap)
can.create_line(     eg,      eh,      eg, eh+y*lc, fill='black', width=lt, capstyle=cap)
can.create_line(eg+x*lc, eh+y*lc, eg+x*lc,      eh, fill='black', width=lt, capstyle=cap)
can.create_line(     eg, eh+y*lc, eg+x*lc, eh+y*lc, fill='black', width=lt, capstyle=cap)

for i,j,door in L.verticalDoors():
    if door: can.create_line(eg+(j+1)*lc, eh+i*lc, eg+(j+1)*lc, eh+(i+1)*lc, fill='black', width=lt, capstyle=cap)
            
for i,j,door in L.horizontalDoors():
    if door: can.create_line(eg+j*lc, eh+(i+1)*lc, eg+(j+1)*lc, eh+(i+1)*lc, fill='black', width=lt, capstyle=cap)
 
 
fen.mainloop()
