from maze import *

### PARAMETRE DU LABYRINTHE :
x   = 10 # nbr de lignes dans le labyrinthe
y   = 10 # nbr de colonnes dans le labyrinthe

L = Maze(x,y,"RB")

print(L)

print(L.toTxt(basic = False, centre= False, coord= False))