from tkinter import *
from random import randrange, choice
from maze import *

### PARAMETRE DU LABYRINTHE :
x   = 100 # nbr de lignes dans le labyrinthe
y   = 100 # nbr de colonnes dans le labyrinthe
eh  = 20 #ecart haut
eg  = 20 #ecart gauche
lc  = 10 #largeur cellule
lt  = 1 #largeur trait
cap = "projecting"
###
cg = randrange(4) # coin gagnant : 0 = haut gauche ; 1 = haut droite ; 2 = bas droite ; 3 = bas gauche
ga = False
if cg == 0:
    Xa, Ya = 0, 0
    Xp, Yp = x-1, y-1
elif cg == 1:
    Xa, Ya = x-1, 0
    Xp, Yp = 0, y-1
elif cg == 2:
    Xa, Ya = x-1, y-1
    Xp, Yp = 0, 0
elif cg == 3:
    Xa, Ya = 0, y-1
    Xp, Yp = x-1, 0

L = Maze(x,y,"RB")


def movement(dire):
    global Xp, Yp, ga
    global Par
    if L.canMove(Xp,Yp, dire) and not ga:
        Xd, Yd = L.move(Xp,Yp,dire)

        if Par[Yd][Xd] == 1  : Par[Yp][Xp] = -1
        else:  Par[Yp][Xp] = 1

        if Par[Yp][Xp] == 1: can.itemconfigure(str(Xp)+","+str(Yp), fill='lightgreen')
        elif Par[Yp][Xp] == -1: can.itemconfigure(str(Xp)+","+str(Yp), fill='pink')
        Xp, Yp = Xd, Yd
        
        can.coords(perso, lc-lt-1+eg+Xp*lc, lc-lt-1+eh+Yp*lc, eg+Xp*lc+lt+1, eh+Yp*lc+lt+1)

    if Xp == Xa and Yp == Ya:
        ga = True
        texte = Label(text="Vous avez Gagné !",relief="ridge",borderwidth = 15,background ="cyan",padx= 10, pady= 10)
        texte.place(x=(eg*2+x*lc)//2, y=(eh*2+y*lc)//2,anchor="center")
    
def fctHaut(ev=None):
    movement("up")
    
def fctBas(ev=None):
    movement("down")
    
def fctDroite(ev=None):
    movement("right")

def fctGauche(ev=None):
    movement("left")
        
    

fen=Tk()
can=Canvas(fen, width=eg*2+x*lc, height=eh*2+y*lc, bg='ivory')
can.pack()

Par = []
for j in range(y):
    l = []
    for i in range(x):
        l.append(0)
        can.create_rectangle(eg+i*lc, eh+j*lc, eg+(i+1)*lc, eh+(j+1)*lc, fill='white', width= 0, tag=str(i)+","+str(j))
    Par.append(l)

can.create_rectangle(eg+Xa*lc, eh+Ya*lc, eg+(Xa+1)*lc, eh+(Ya+1)*lc, fill='#00C82F', width= 0)

can.create_line(     eg,      eh, eg+x*lc,      eh, fill='black', width=lt, capstyle=cap)
can.create_line(     eg,      eh,      eg, eh+y*lc, fill='black', width=lt, capstyle=cap)
can.create_line(eg+x*lc, eh+y*lc, eg+x*lc,      eh, fill='black', width=lt, capstyle=cap)
can.create_line(     eg, eh+y*lc, eg+x*lc, eh+y*lc, fill='black', width=lt, capstyle=cap)

for i,j,door in L.verticalDoors():
    if door: can.create_line(eg+(j+1)*lc, eh+i*lc, eg+(j+1)*lc, eh+(i+1)*lc, fill='black', width=lt, capstyle=cap)
            
for i,j,door in L.horizontalDoors():
    if door: can.create_line(eg+j*lc, eh+(i+1)*lc, eg+(j+1)*lc, eh+(i+1)*lc, fill='black', width=lt, capstyle=cap)


perso = can.create_oval(lc-lt-1+eg+Xp*lc, lc-lt-1+eh+Yp*lc, eg+Xp*lc+lt+1, eh+Yp*lc+lt+1, fill='red',width=0)

can.bind_all('<Up>',fctHaut)
can.bind_all('<Down>',fctBas)
can.bind_all('<Right>',fctDroite)
can.bind_all('<Left>',fctGauche)
 
fen.mainloop()
