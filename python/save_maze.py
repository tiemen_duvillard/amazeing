from maze import *

### PARAMETRE DU LABYRINTHE :
x   = 5 # nbr de lignes dans le labyrinthe
y   = 5 # nbr de colonnes dans le labyrinthe

L = Maze(x,y,"K")

print(L.toTxt())

info = L.save()
print(info)
# write info in file, etc...

H = Maze()
H.load(info)
print(H.toTxt())
