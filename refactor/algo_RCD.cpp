

void DecoupeRec(Laby * labyrinthe, int xA, int yA, int xB, int yB) {
    if (xB - xA <= 1) { return; }
    else if (yB - yA <= 1) { return; }
    else {
        int lx = xB - xA;
        int ly = yB - yA;
        int v = randomRange(0, lx+ly);
        if (v < lx) { // on coupe en 2 verticalement
            int x = randomRange(xA, xB-1);
            int y = randomRange(yA, yB);
            for (int i = yA; i < yB; i ++) {
                if (i != y) { labyrinthe->setP_Verti(x, i, 1);  }
            }
            DecoupeRec(labyrinthe, xA, yA, x+1, yB);
            DecoupeRec(labyrinthe, x+1, yA, xB, yB);
        } else { // on coupe en 2 horizontalement
            int x = randomRange(xA, xB);
            int y = randomRange(yA, yB-1);
            for (int i = xA; i < xB; i ++) {
                if (i != x) { labyrinthe->setP_Horiz(i, y, 1);  }
            }
            DecoupeRec(labyrinthe, xA, yA, xB, y+1);
            DecoupeRec(labyrinthe, xA, y+1, xB, yB);
        }
    }
}



void Laby::genere_RCD() {
        // affectation des portes Horizontales
    for (int y = 0; y < this->getP_HY(); y++ ) {
        for (int x = 0; x < this->getP_HX(); x++ ) {
            this->setP_Horiz(x,y,0);
        }
    }
    // affectation des portes Verticales
    for (int y = 0; y < this->getP_VY(); y++ ) {
        for (int x = 0; x < this->getP_VX(); x++ ) {
            this->setP_Verti(x,y,0);
        }
    }
    DecoupeRec(this,0,0, this->getLargeur(),this->getHauteur());
}


