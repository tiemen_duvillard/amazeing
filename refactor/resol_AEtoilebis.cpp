


void Laby::resout_Aetoilebis(Resolution *reso, int xS, int yS, int xE, int yE) {

    int CASES[this->getLargeur()][this->getHauteur()];
    for (int x = 0; x < this->getLargeur() ; x++ ) {
        for (int y = 0; y < this->getHauteur() ; y++ ) {
            CASES[x][y] = -1;
        }
    }

    typedef struct pos { int x,y,d; } pos ;

    auto compare = [](pos p1, pos p2) { 
        if (p1.d < p2.d)  { return -1; }
        if (p1.d == p2.d) { return  0; }
        if (p1.d > p2.d)  { return  1; }
    };


    priority_queue <pos, vector<pos>, decltype(compare)> pile(compare);
    pos p;

    CASES[xS][yS] = 0;
    
    p.x = xS;
    p.y = yS;
    p.d = dManha(xS,yS,xE,yE);
    pile.push(p);
    int nbStep = 0;
    auto start = std::chrono::high_resolution_clock::now();

    int xa,ya;
    while (! pile.empty() && CASES[xE][yE] == -1 ) {
        nbStep ++;
        p = pile.top();
        pile.pop();
        xa = p.x;
        ya = p.y;

        if (this->up(xa, ya)) {
            if (CASES[xa][ya-1] == -1) {
                CASES[xa][ya-1] = CASES[xa][ya] +1;
                p.x = xa;
                p.y = ya-1;
                p.d = dManha(p.x,p.y,xE,yE); 
                pile.push(p);
            }
        }
        if (this->down(xa, ya)) {
            if (CASES[xa][ya+1] == -1) {
                CASES[xa][ya+1] = CASES[xa][ya] +1;
                p.x = xa;
                p.y = ya+1;
                p.d = dManha(p.x,p.y,xE,yE); 
                pile.push(p);
            }
        }
        if (this->left(xa, ya)) {
            if (CASES[xa-1][ya] == -1) {
                CASES[xa-1][ya] = CASES[xa][ya] +1;
                p.x = xa-1;
                p.y = ya;
                p.d = dManha(p.x,p.y,xE,yE); 
                pile.push(p);
            }
        }
        if (this->right(xa, ya)) {
            if (CASES[xa+1][ya] == -1) {
                CASES[xa+1][ya] = CASES[xa][ya] +1;
                p.x = xa+1;
                p.y = ya;
                p.d = dManha(p.x,p.y,xE,yE); 
                pile.push(p);
            }
        }

    }

    // for (int y = 0; y < this->getHauteur(); y ++) {
    //     for (int x = 0; x < this->getLargeur(); x ++) {
    //         cout << CASES[x][y] << " ";
    //     }
    //     cout << endl;
    // }
    // cout << endl;
    // cout << endl;

    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_ms = end - start;
    if (pile.empty()) {reso->possible = false; }
    else { reso->possible = true; }
    reso->lenChe = CASES[xE][yE];
    reso->nbExplore = nbStep;
    reso->time = float_ms.count();
    

}