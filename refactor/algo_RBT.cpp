


void Laby::genere_RBT() {
    
    // affectation des portes Horizontales
    for (int y = 0; y < this->getP_HY(); y++ ) {
        for (int x = 0; x < this->getP_HX(); x++ ) {
            this->setP_Horiz(x,y,1);
        }
    }
    // affectation des portes Verticales
    for (int y = 0; y < this->getP_VY(); y++ ) {
        for (int x = 0; x < this->getP_VX(); x++ ) {
            this->setP_Verti(x,y,1);
        }
    }


    bool CASES[this->getLargeur()][this->getHauteur()];
    for (int x = 0; x < this->getLargeur(); ++x) {
        for (int y = 0; y < this->getHauteur(); ++y) {
            CASES[x][y] = true;
        }
    }


    typedef struct pos { int x,y; } pos ;
    stack<pos> pile;
    pos actu;
    pos next;

    actu.x = randomRange(0,this->getLargeur());
    actu.y = randomRange(0,this->getHauteur());

    pile.push(actu);

    int choix_pos[4];
    int nb_pos;
    int choix;
    int n;


    while (!pile.empty()) {
        
        actu = pile.top();
        CASES[actu.x][actu.y] = false;

        choix_pos[4];
        nb_pos = 0;
        
        if (actu.y > 0) {                    if ( CASES[actu.x][actu.y-1] ) { choix_pos[nb_pos] = 0; nb_pos += 1; } }
        if (actu.x < this->getLargeur()-1) { if ( CASES[actu.x+1][actu.y] ) { choix_pos[nb_pos] = 1; nb_pos += 1; } }
        if (actu.y < this->getHauteur()-1) { if ( CASES[actu.x][actu.y+1] ) { choix_pos[nb_pos] = 2; nb_pos += 1; } }
        if (actu.x > 0) {                    if ( CASES[actu.x-1][actu.y] ) { choix_pos[nb_pos] = 3; nb_pos += 1; } }


        if (nb_pos == 0) {
            pile.pop();
        } else if (nb_pos == 1) {
            choix = choix_pos[0];
            if (choix == 0 ) { next.x = actu.x   ; next.y = actu.y-1 ; this->setP_Horiz(actu.x,actu.y-1,0); }
            if (choix == 1 ) { next.x = actu.x+1 ; next.y = actu.y   ; this->setP_Verti(actu.x,actu.y,0);   }
            if (choix == 2 ) { next.x = actu.x   ; next.y = actu.y+1 ; this->setP_Horiz(actu.x,actu.y,0);   }
            if (choix == 3 ) { next.x = actu.x-1 ; next.y = actu.y   ; this->setP_Verti(actu.x-1,actu.y,0); }

            pile.pop();
            pile.push(next);
        } else {
            n = randomRange(0,nb_pos);
            choix = choix_pos[n];
            if (choix == 0 ) { next.x = actu.x   ; next.y = actu.y-1 ; this->setP_Horiz(actu.x,actu.y-1,0); }
            if (choix == 1 ) { next.x = actu.x+1 ; next.y = actu.y   ; this->setP_Verti(actu.x,actu.y,0);   }
            if (choix == 2 ) { next.x = actu.x   ; next.y = actu.y+1 ; this->setP_Horiz(actu.x,actu.y,0);   }
            if (choix == 3 ) { next.x = actu.x-1 ; next.y = actu.y   ; this->setP_Verti(actu.x-1,actu.y,0); }
            pile.push(next);
        }   


    }
}
