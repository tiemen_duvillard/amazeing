
struct Scellule {
    bool is_represent;
    struct Scellule * parent;
}  ;
typedef struct Scellule cellule;


void init(cellule * C) {
    C->is_represent = true;
    C->parent = NULL;
}

cellule* reprensentant(cellule * C, int * d) {
    *d = 0;
    cellule* next = C;
    while (!next->is_represent) {
        next = next->parent;
        *d += 1;
    }
    return next;
}



typedef struct {
    bool is_verti;
    int x;
    int y;      
} Eporte ;



void shufflePortes(Eporte *array, int n) {
    if (n > 1) {
        int i;
        for (i = 0; i < n - 1; i++) {
            int j = i + rand() / (RAND_MAX / (n - i) + 1);
            Eporte t = array[j];
            array[j] = array[i];
            array[i] = t;
        }
    }
}


void Laby::genere_kruskal() {

    // affectation des portes Horizontales
    for (int y = 0; y < this->getP_HY(); y++ ) {
        for (int x = 0; x < this->getP_HX(); x++ ) {
            this->setP_Horiz(x,y,1);
        }
    }
    // affectation des portes Verticales
    for (int y = 0; y < this->getP_VY(); y++ ) {
        for (int x = 0; x < this->getP_VX(); x++ ) {
            this->setP_Verti(x,y,1);
        }
    }

    int x1,y1; cellule* r1; int d1;
    int x2,y2; cellule* r2; int d2;
        

    cellule CASES[this->getLargeur() * this->getHauteur()];

    Eporte p;

    for (int x = 0; x <  this->getLargeur(); ++x) {
        for (int y = 0; y < this->getHauteur(); ++y) {
            int i = x * this->getHauteur() + y;
            init(& CASES[i]);
        }
    }
    
    Eporte* Vportes = new Eporte [this->getP_HX() * this->getP_HY() + this->getP_VX() * this->getP_VY()];
   
  
    int NB_PORTE = 0;
    p.is_verti = false;
    for (int x = 0; x < this->getP_HX(); ++x) {
        for (int y = 0; y < this->getP_HY(); ++y) {
            p.x = x; p.y = y;
            Vportes[NB_PORTE] = p;
            NB_PORTE++;
        }
    }
    p.is_verti = true;
    for (int x = 0; x < this->getP_VX(); ++x) {
        for (int y = 0; y < this->getP_VY(); ++y) {
            p.x = x; p.y = y;
            Vportes[NB_PORTE] = p;
            NB_PORTE++;
        }
    }

    shufflePortes(Vportes, NB_PORTE); 


    int CPT_PORTE = this->getHauteur() * this->getLargeur() - 1;

    while (CPT_PORTE > 0) {

        p = Vportes[NB_PORTE-1];
        NB_PORTE -= 1;




        if (p.is_verti) {
            x1 = p.x; y1 = p.y;
            x2 = p.x+1; y2 = p.y;
        } else {
            x1 = p.x; y1 = p.y;
            x2 = p.x; y2 = p.y+1;
        }

        r1 = reprensentant(& CASES[x1 * this->getHauteur() + y1], & d1);
        r2 = reprensentant(& CASES[x2 * this->getHauteur() + y2], & d2);


        if (r1 != r2) {
            if (d1 < d2) {
                r1->is_represent = false;
                r1->parent = r2;
            } else {
                r2->is_represent = false;
                r2->parent = r1;
            }

            CPT_PORTE -= 1 ;

            if (p.is_verti) {
                this->setP_Verti(p.x, p.y, 0);
            } else {
                this->setP_Horiz(p.x, p.y, 0);
            }
        }
    }

}
