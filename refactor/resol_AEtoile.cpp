#include <limits>
#include<chrono>
#include <map>
#include <list>
#include <unistd.h>

using namespace std;

class Cell
{
private:
    int x;
    int y;
    int g;
    int h;
    Cell* parent; //in shortest path


public:
    //x : abscysse ; y : ordonnée
    Cell(int x, int y);

    int get_x();
    int get_y();

    void set_g(int gval);
    void set_h(int hval);

    int get_g();
    int get_h();
    int get_f();

    void set_parent(Cell* parent);
    Cell* get_parent();

    string to_string();
    string pos_string();
};

//Cell constructor

Cell::Cell(int j, int i)
{
    this->x = j;
    this->y = i;
    this->g = std::numeric_limits<int>::max(); // infiniment loin de l'entree
    this->h = std::numeric_limits<int>::max(); // infiniment loin de la sortie
}


// methods for computing cost

void Cell::set_g(int gval)
{
    this->g = gval;
}

void Cell::set_h(int hval)
{
    this->h = hval;
}

int Cell::get_g() { return this->g; }
int Cell::get_h() { return this->h; }
int Cell::get_f() { return this->g + this->h; }

int Cell::get_x() { return this->x; }
int Cell::get_y() { return this->y; }

void Cell::set_parent(Cell* parent)
{
    this->parent = parent;
}

Cell* Cell::get_parent()
{
    return this->parent;
}

string Cell::to_string()
{
    return "[" + std::to_string(x) + ", " + std::to_string(y) + " | f: " + std::to_string(g + h) + "]";
}

string Cell::pos_string()
{
    return "(" + std::to_string(x) + ", " + std::to_string(y) + ")";
}

int dist_manhattan(Cell* c1, Cell* c2)
{
    return abs(c2->get_y() - c1->get_y()) + abs(c2->get_x() - c1->get_x());
}

int final_path_length(Cell* goal_cell, Cell* start_cell)
{
    int len_path = 0;
    Cell* par = goal_cell->get_parent();
    //while (par != NULL)
    while (par->get_x() != start_cell->get_x() || par->get_y() != start_cell->get_y())
    {
        len_path++;
        par = par->get_parent();
    }
    
    return len_path;
}

bool better_parent(list<Cell*> l, Cell* cell)
{
    bool res = false;
    for (list<Cell*>::iterator it = l.begin(); it != l.end(); ++it)
    {
        Cell* it_cell = *it;
        if (it_cell->get_x() == cell->get_x() && it_cell->get_y() == cell->get_y())
        {
            if (it_cell->get_f() < cell->get_f())
            {
                res = true;
            }
        }
    }
    return res;
}

string list_to_string(list<Cell*> l) {
    string res = "";
    if (!l.empty())
    {
        for (list<Cell*>::iterator it = l.begin(); it != l.end(); ++it)
        {
            Cell* it_cell = *it;
            res += it_cell->to_string() + ", ";
        }
    }
    else
    {
        cout << "ERROR : LIST IS EMPTY" << endl;
    }
    return res;
}

void Laby::resout_Aetoile(Resolution* reso, int xS, int yS, int xE, int yE)
{
    //usleep(5000);
    int interX = xS; xS = xE; xE = interX;
    int interY = yS; yS = yE; yE = interY;

    auto start = std::chrono::high_resolution_clock::now();

    int nbStep = 0;

    Cell* start_cell = new Cell(xE, yE);
    Cell* goal_cell = new Cell(xS, yS);

    start_cell->set_parent(NULL);
    start_cell->set_g(0);
    start_cell->set_h(dist_manhattan(start_cell, goal_cell));

    goal_cell->set_h(0);

    list<Cell*> open;
    list<Cell*> closed;

    open.push_back(start_cell);

    while (!open.empty())
    {
        nbStep++;

        /*if (nbStep == 17) {
            cout << "time is up" << endl;
            break;
        }*/


        int min = (*open.begin())->get_f();

        Cell* q;
        list<Cell*>::iterator q_it;

        //Finding q with minimal f

        for (list<Cell*>::iterator it = open.begin(); it != open.end(); ++it)
        {
            Cell* it_cell = *it;

            if (min >= it_cell->get_f())
            {
                min = it_cell->get_f();
                q = it_cell;
                q_it = it;
                //cout << "New Min: " << q->to_string() << endl;

                //get pointer towards elt
            }
        }
        //cout << endl;

        //affichage open initial

        //cout << "Noeud current : " << q->to_string() << endl;

        open.erase(q_it);

        // Generating successors of q

        list<Cell*> successors;

        if (this->up(q->get_x(), q->get_y()))
        {
            Cell* next = new Cell(q->get_x(), q->get_y() - 1);
            next->set_parent(q);
            successors.push_back(next);
        }
        if (this->down(q->get_x(), q->get_y()))
        {
            Cell* next = new Cell(q->get_x(), q->get_y() + 1);
            next->set_parent(q);
            successors.push_back(next);
        }
        if (this->left(q->get_x(), q->get_y()))
        {
            Cell* next = new Cell(q->get_x() - 1, q->get_y());
            next->set_parent(q);
            successors.push_back(next);
        }
        if (this->right(q->get_x(), q->get_y()))
        {
            Cell* next = new Cell(q->get_x() + 1, q->get_y());
            next->set_parent(q);
            successors.push_back(next);
        }

        for (list<Cell*>::iterator it = successors.begin(); it != successors.end(); ++it)
        {
            Cell* succ = *it;

            //GOAAAAAAAAAAAAAAAAAAAL ATTEINT

            if (xS == succ->get_x() && yS == succ->get_y())
            {
                succ->set_g(q->get_g() + 1);
                Cell* par = succ->get_parent();

                while (par != NULL)
                {
                    // cout << par->pos_string();
                    par = par->get_parent();
                }
                // cout << endl;

                //first we stop timecount

                auto end = std::chrono::high_resolution_clock::now();

                //We set reso time 

                std::chrono::duration<double, std::milli> float_ms = end - start;
                reso->time = float_ms.count();


                // Get length of total path length ERROR
                
                reso->lenChe = 1+final_path_length(succ, start_cell);

                // Set the number of explored nodes to the number of explored states in OPEN
                reso->nbExplore = 1+nbStep;

                reso->possible = true;

                //break;
                return;
            }

            // NOT GOAL -> CONTINUE EXPLORATION

            succ->set_g(q->get_g() + 1); //Rajouter 1 pour prendre en compte la longueur du chemin pour arriver en succ
            succ->set_h(dist_manhattan(succ, goal_cell));

            if (!better_parent(open, succ))
            {
                if (!better_parent(closed, succ))
                {
                    open.push_back(succ);
                    //cout << "Successeur accepté à explorer : " << succ->to_string() << endl;
                }
                /*else {
                    cout << "CLOSED with better cost" << endl;
                }*/
            }
            /*else {
                cout << "OPENED with better cost" << endl;
            }*/

        }

        closed.push_back(q);

        //cout << "OPEN : " <<list_to_string(open) << endl;
        //cout << "CLOSED : " <<list_to_string(closed) << endl;

    }

    if (!reso->possible)
    {
        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double, std::milli> float_ms = end - start;
        reso->time = float_ms.count();
        reso->nbExplore = nbStep;
    }

}