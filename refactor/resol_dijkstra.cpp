
void Laby::resout_dijkstra(Resolution * reso, int xS, int yS, int xE, int yE) {



    int CASES[this->getLargeur()][this->getHauteur()];
    for (int x = 0; x < this->getLargeur() ; x++ ) {
        for (int y = 0; y < this->getHauteur() ; y++ ) {
            CASES[x][y] = -1;
        }
    }

    int xa,ya;

    auto start = std::chrono::high_resolution_clock::now();
    typedef struct pos { int x,y; } pos ;
    queue<pos> pile;
    pos p;

    CASES[xS][yS] = 0;
    p.x = xS;
    p.y = yS;
    pile.push(p);
    int ETAPE = 0;
    while (! pile.empty() && CASES[xE][yE] == -1 ) {
        ETAPE ++;
        p = pile.front();
        pile.pop();
        xa = p.x;
        ya = p.y;

        if (this->up(xa, ya)) {
            if (CASES[xa][ya-1] == -1) {
                CASES[xa][ya-1] = CASES[xa][ya] +1;
                p.x = xa;
                p.y = ya-1;
                pile.push(p);
            }
        }
        if (this->down(xa, ya)) {
            if (CASES[xa][ya+1] == -1) {
                CASES[xa][ya+1] = CASES[xa][ya] +1;
                p.x = xa;
                p.y = ya+1;
                pile.push(p);
            }
        }
        if (this->left(xa, ya)) {
            if (CASES[xa-1][ya] == -1) {
                CASES[xa-1][ya] = CASES[xa][ya] +1;
                p.x = xa-1;
                p.y = ya;
                pile.push(p);
            }
        }
        if (this->right(xa, ya)) {
            if (CASES[xa+1][ya] == -1) {
                CASES[xa+1][ya] = CASES[xa][ya] +1;
                p.x = xa+1;
                p.y = ya;
                pile.push(p);
            }
        }
    }
    
    // for (int y = 0; y < this->getHauteur(); y ++) {
    // for (int x = 0; x < this->getLargeur(); x ++) {
    //     cout << CASES[x][y] << " ";
    // }
    // cout << endl;
    // }
    // cout << endl;
    // cout << endl;


    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_ms = end - start;
    if (pile.empty()) {reso->possible = false; }
    else { reso->possible = true; }
    reso->lenChe = CASES[xE][yE];
    reso->nbExplore = ETAPE;
    reso->time = float_ms.count();

}