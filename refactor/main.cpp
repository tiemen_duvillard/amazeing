#include "laby.h"





// ./laby [q] [t] X Y algo_genere [c N] [s algo_solve]
// -q : Pour ne pas afficher le labyrinthe en txt
// -t : Pour afficher les stats de géneration / résolution
// -c N  : Pour ouvrir N portes
// -s algo_solve  : Pour résoudre le labyrinthe avec un algo

int main(int argc, char* argv[]) {
    srand(time(NULL));
	if (argc < 4) {
		cout << "Options necessaire : X Y algo_generation" << endl;
		return 1;
	}
	bool QUIET = false;
	bool STAT  = false;

	int i = 1;
	if (argv[i][0] == 'q') { QUIET  = true; i++; }
	if (argv[i][0] == 't') { STAT   = true; i++; }

	int X = atoi(argv[i]);
	int Y = atoi(argv[i+1]);
	char* algoG = argv[i+2];
	i += 3;

    auto start = std::chrono::high_resolution_clock::now();

	Laby *myLaby = new Laby(X,Y,algoG);

	if (argc > i+1) {
		if (argv[i][0] == 'c') {
			int N = atoi(argv[i+1]);
			myLaby->ouvrePorte(N);
			i += 2;
		}
	}
	auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_ms = end - start;

	if (! QUIET) { cout << myLaby->toTxt() << endl;	}

	if (STAT) {
		cout << endl;
		cout << "Stats de generation :" << endl; // Temps etc..
		cout << " Algo = " << algoG <<endl;
		cout << " Largeur = " << X <<endl;
		cout << " Hauteur = " << Y <<endl;
		cout << " Temps de generation (ms) = " << float_ms.count() <<endl;
	}
	if (STAT) {
		cout << endl;
		cout << "Stats du labyrinthe :" << endl; // diagonale, portes..
		cout << " Diagonale 1 = " << myLaby->lenPlusCourtChemin(0,0,X-1,Y-1) << endl;
		cout << " Diagonale 2 = " << myLaby->lenPlusCourtChemin(0,Y-1,X-1,0) << endl;
		cout << " Diametre    = " << myLaby->plusLongChemin() << endl;
		int p0 = 0; int p1 = 0; int p2 = 0; int p3 = 0; int p4 = 0;
		myLaby->repartitionPortes(& p0,& p1,& p2,& p3,& p4);
		cout << " Nbr de case avec 0 porte  = " << p0 << endl;
		cout << " Nbr de case avec 1 porte  = " << p1 << endl;
		cout << " Nbr de case avec 2 portes = " << p2 << endl;
		cout << " Nbr de case avec 3 portes = " << p3 << endl;
		cout << " Nbr de case avec 4 portes = " << p4 << endl;
	}
	

	if (argc > i+1) {
		if (argv[i][0] == 's') {
			char * algoS = argv[i+1];
			Resolution myResol;
			myLaby->resout(& myResol,0,0,X-1,Y-1,algoS);
			i += 2;

			if (STAT) {
				cout << endl;
				cout << "Stats de la résolution :" << endl; // temps, pas
				cout << " Trajet de (0,0) vers (" << X-1 << "," << Y-1 << ")"<< endl;
				if (myResol.possible) {
					cout << " Les cases sont reliables." << endl;
					cout << " Longueur du chemin trouvé = " << myResol.lenChe << endl; 
				} else {
					cout << " Les cases ne sont pas reliables." << endl;
				}
				cout << " Nbr d'etape   = " << myResol.nbExplore << endl;
				cout << " Temps (ms)    = " << myResol.time << endl;
			}
		}
	}

	return 0;
}