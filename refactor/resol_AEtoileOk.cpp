

typedef struct celluleAE {
    bool visite;
    int x,y,d,c;
    bool up,right,down,left;
} celluleAE ;

typedef struct pos { int x,y; celluleAE* adr; } pos ;

int acces(int taille, int x, int y) {
    return taille*y +x;
}
void MAJ(int taille, celluleAE* TAB, int x, int y , int nv_cout) {
    cout << "la case "<< x <<" "<<y<< "a ete mis à jour aec le cout "<<nv_cout<<endl;
    if (TAB[acces(taille,x,y)].c > nv_cout) {TAB[acces(taille,x,y)].c = nv_cout;}
    if (TAB[acces(taille,x,y)].up     && TAB[acces(taille,x, y-1)].c +1 > nv_cout ) { MAJ(taille,TAB, x, y-1 , nv_cout+1); }
    if (TAB[acces(taille,x,y)].down   && TAB[acces(taille,x, y+1)].c +1 > nv_cout ) { MAJ(taille,TAB, x, y+1 , nv_cout+1); }
    if (TAB[acces(taille,x,y)].left   && TAB[acces(taille,x-1, y)].c +1 > nv_cout ) { MAJ(taille,TAB, x-1, y , nv_cout+1); }
    if (TAB[acces(taille,x,y)].right  && TAB[acces(taille,x+1, y)].c +1 > nv_cout ) { MAJ(taille,TAB, x+1, y , nv_cout+1); }
}


void Laby::resout_AetoileOk(Resolution *reso, int xS, int yS, int xE, int yE) {
    int SIz = this->getLargeur(); 

    celluleAE CASES[this->getLargeur() * this->getHauteur()];
    for (int x = 0; x < this->getLargeur() ; x++ ) {
        for (int y = 0; y < this->getHauteur() ; y++ ) {
            CASES[acces(SIz,x,y)].visite = false;
        }
    }


    auto compare = [](pos p1, pos p2) {
        if ( p1.adr->c + p1.adr->d < p2.adr->c + p2.adr->d) {
            return -1;
        }
        if ( p1.adr->c + p1.adr->d == p2.adr->c + p2.adr->d) {
            return  0;
        }
        if ( p1.adr->c + p1.adr->d > p2.adr->c + p2.adr->d) {
            return  1;
        }
    };


    priority_queue <pos, vector<pos>, decltype(compare)> file(compare);
    pos p;
    celluleAE cel;
    p.x = xS;
    p.y = yS;
    p.adr = & (CASES[acces(SIz,p.x,p.y)]);

    cel.x = xS;
    cel.y = yS;
    cel.c = 0;
    cel.d = dManha(xS,yS,xE,yE);
    cel.up = false;
    cel.right = false;
    cel.down = false;
    cel.left = false;
    cel.visite = true;

    CASES[acces(SIz,xS,yS)] = cel;

    file.push(p);
    int nbStep = 0;
    auto start = std::chrono::high_resolution_clock::now();

    int xa,ya;
    while (! file.empty() && CASES[acces(SIz,xE,yE)].visite == false ) {
        nbStep ++;
        p = file.top();
        file.pop();
        xa = p.x;
        ya = p.y;

        if (this->up(xa, ya)) {
            if (CASES[acces(SIz,xa,ya-1)].visite == false) {

                CASES[acces(SIz,xa,ya-1)].x = xa;
                CASES[acces(SIz,xa,ya-1)].y = ya-1;
                CASES[acces(SIz,xa,ya-1)].c = CASES[acces(SIz,xa,ya)].c + 1;
                CASES[acces(SIz,xa,ya-1)].d = dManha(xa,ya-1,xE,yE);
                CASES[acces(SIz,xa,ya-1)].up = false;
                CASES[acces(SIz,xa,ya-1)].right = false;
                CASES[acces(SIz,xa,ya-1)].down = false;
                CASES[acces(SIz,xa,ya-1)].left = false;
                CASES[acces(SIz,xa,ya-1)].visite = true;

                CASES[acces(SIz,xa,ya)].up = true;

                p.x = xa;
                p.y = ya-1;
                p.adr = & (CASES[acces(SIz,p.x,p.y)]);
                file.push(p);
            } else {
                if (CASES[acces(SIz,xa,ya-1)].c > CASES[acces(SIz,xa,ya)].c + 1) {
                    cout << "mise à jour"<<endl;
                    cout << xa << " " << ya-1 << endl;
                    cout << CASES[acces(SIz,xa,ya-1)].c <<" <> "<< CASES[acces(SIz,xa,ya)].c + 1 <<endl;
                    MAJ(SIz,CASES,xa,ya-1,CASES[acces(SIz,xa,ya)].c +1);

                }
            }
        }
        if (this->down(xa, ya)) {
            if (CASES[acces(SIz,xa,ya+1)].visite == false) {

                CASES[acces(SIz,xa,ya+1)].x = xa;
                CASES[acces(SIz,xa,ya+1)].y = ya+1;
                CASES[acces(SIz,xa,ya+1)].c = CASES[acces(SIz,xa,ya)].c + 1;
                CASES[acces(SIz,xa,ya+1)].d = dManha(xa,ya+1,xE,yE);
                CASES[acces(SIz,xa,ya+1)].up = false;
                CASES[acces(SIz,xa,ya+1)].right = false;
                CASES[acces(SIz,xa,ya+1)].down = false;
                CASES[acces(SIz,xa,ya+1)].left = false;
                CASES[acces(SIz,xa,ya+1)].visite = true;

                CASES[acces(SIz,xa,ya)].down = true;

                p.x = xa;
                p.y = ya+1;
                p.adr = & (CASES[acces(SIz,p.x,p.y)]);
                file.push(p);
            } else {
                if (CASES[acces(SIz,xa,ya+1)].c > CASES[acces(SIz,xa,ya)].c + 1) {
                    cout << "mise à jour"<<endl;
                    cout << xa << " " << ya+1 << endl;
                    cout << CASES[acces(SIz,xa,ya+1)].c <<" <> "<< CASES[acces(SIz,xa,ya)].c + 1 <<endl;
                    MAJ(SIz,CASES,xa,ya+1,CASES[acces(SIz,xa,ya)].c +1);

                }
            }
        }
        if (this->left(xa, ya)) {
            if (CASES[acces(SIz,xa-1,ya)].visite == false) {

                CASES[acces(SIz,xa-1,ya)].x = xa-1;
                CASES[acces(SIz,xa-1,ya)].y = ya;
                CASES[acces(SIz,xa-1,ya)].c = CASES[acces(SIz,xa,ya)].c + 1;
                CASES[acces(SIz,xa-1,ya)].d = dManha(xa-1,ya,xE,yE);
                CASES[acces(SIz,xa-1,ya)].up = false;
                CASES[acces(SIz,xa-1,ya)].right = false;
                CASES[acces(SIz,xa-1,ya)].down = false;
                CASES[acces(SIz,xa-1,ya)].left = false;
                CASES[acces(SIz,xa-1,ya)].visite = true;

                CASES[acces(SIz,xa,ya)].left = true;

                p.x = xa-1;
                p.y = ya;
                p.adr = & (CASES[acces(SIz,p.x,p.y)]);
                file.push(p);
            } else {
                if (CASES[acces(SIz,xa-1,ya)].c > CASES[acces(SIz,xa,ya)].c + 1) {
                    cout << "mise à jour"<<endl;
                    cout << xa-1 << " " << ya << endl;
                    cout << CASES[acces(SIz,xa-1,ya)].c <<" <> "<< CASES[acces(SIz,xa,ya)].c + 1 <<endl;
                    MAJ(SIz,CASES,xa-1,ya,CASES[acces(SIz,xa,ya)].c +1);

                }
            }
        }
        if (this->right(xa, ya)) {
            if (CASES[acces(SIz,xa+1,ya)].visite == false) {

                CASES[acces(SIz,xa+1,ya)].x = xa+1;
                CASES[acces(SIz,xa+1,ya)].y = ya;
                CASES[acces(SIz,xa+1,ya)].c = CASES[acces(SIz,xa,ya)].c + 1;
                CASES[acces(SIz,xa+1,ya)].d = dManha(xa+1,ya,xE,yE);
                CASES[acces(SIz,xa+1,ya)].up = false;
                CASES[acces(SIz,xa+1,ya)].right = false;
                CASES[acces(SIz,xa+1,ya)].down = false;
                CASES[acces(SIz,xa+1,ya)].left = false;
                CASES[acces(SIz,xa+1,ya)].visite = true;

                CASES[acces(SIz,xa,ya)].right = true;

                p.x = xa+1;
                p.y = ya;
                p.adr = & (CASES[acces(SIz,p.x,p.y)]);
                file.push(p);
            } else {
                if (CASES[acces(SIz,xa+1,ya)].c > CASES[acces(SIz,xa,ya)].c + 1) {
                    cout << "mise à jour"<<endl;
                    cout << xa+1 << " " << ya << endl;
                    cout << CASES[acces(SIz,xa+1,ya)].c <<" <> "<< CASES[acces(SIz,xa,ya)].c + 1 <<endl;
                    MAJ(SIz,CASES,xa+1,ya,CASES[acces(SIz,xa,ya)].c +1);

                }
            }
        }

    }

    // for (int y = 0; y < this->getHauteur(); y ++) {
    //     for (int x = 0; x < this->getLargeur(); x ++) {
            //cout << CASES[x][y] << " ";
    //     }
        //cout << endl;
    // }
    //cout << endl;
    //cout << endl;

    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> float_ms = end - start;
    if (file.empty()) {reso->possible = false; }
    else { reso->possible = true; }
    reso->lenChe = CASES[acces(SIz,xE,yE)].c;
    reso->nbExplore = nbStep;
    reso->time = float_ms.count();
    

}