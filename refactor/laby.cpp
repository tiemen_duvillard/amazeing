
#include "laby.h"

float randomFloat() { return (float) rand() / (float) RAND_MAX; }
int randomRange(int mini, int maxi) { return rand()%(maxi - mini) + mini; }

int dManha(int x1, int y1, int x2, int y2) {
    int dx = x1 - x2;
    int dy = y1 - y2;
    if (dx < 0) { dx = -dx;}
    if (dy < 0) { dy = -dy;}
    return dx + dy;
}


Laby::Laby(int Largeur, int Hauteur, string name_algo) {
    // initialisation des bornes
    this->Largeur = Largeur;
    this->Hauteur = Hauteur;
    this->P_HX = this->getLargeur();
    this->P_HY = this->getHauteur()-1;
    this->P_VX = this->getLargeur()-1;
    this->P_VY = this->getHauteur();

    // initialisation des tableaux
    this->P_Horiz =  new porte[this->getP_HY() * this->getP_HX()];
    this->P_Verti =  new porte[this->getP_VY() * this->getP_VX()];

    // Generation des portes
    transform(name_algo.begin(), name_algo.end(), name_algo.begin(), ::tolower);

    if ((name_algo == "naif") || (name_algo == "n")) {
        this->genere_naif();
    }
    if ((name_algo == "empty") || (name_algo == "e")) {
        this->genere_empty();
    }
    if ((name_algo == "full") || (name_algo == "f")) {
        this->genere_full();
    }
    if ((name_algo == "kruskal") || (name_algo == "k")) {
        this->genere_kruskal();
    }
    if ((name_algo == "recursive_backtrack") || (name_algo == "rbt")) {
        this->genere_RBT();
    }
    if ((name_algo == "recursive_chamber") || (name_algo == "recursive_chamber_division") || (name_algo == "rcd")) {
        this->genere_RCD();
    }
}
Laby::~Laby() {
    free(this->P_Horiz);
    free(this->P_Verti);
}
int Laby::getLargeur() { return this->Largeur; }
int Laby::getHauteur() { return this->Hauteur; }

int Laby::getP_VY() { return this->P_VY; }
int Laby::getP_VX() { return this->P_VX; }
porte Laby::getP_Verti(int x, int y) {
    if (x < 0 || x >= this->getP_VX() || y < 0 || y >= this->getP_VY() ){ cout << "ERREUR1 : on essaye d'acceder à la porte verti ("<<x<<","<<y<<")" << endl; }
    return this->P_Verti[x + (this->getP_VX() * y)];
}
void Laby::setP_Verti(int x, int y, porte val) {
    if (x < 0 || x >= this->getP_VX() || y < 0 || y >= this->getP_VY() ){ cout << "ERREUR2 : on essaye de modifier la porte verti ("<<x<<","<<y<<")" << endl; }
    this->P_Verti[x + (this->getP_VX() * y)] = val;
}

int Laby::getP_HY() { return this->P_HY; }
int Laby::getP_HX() { return this->P_HX; }
porte Laby::getP_Horiz(int x, int y) {
    if (x < 0 || x >= this->getP_HX() || y < 0 || y >= this->getP_HY() ){ cout << "ERREUR3 : on essaye d'acceder à la porte horiz ("<<x<<","<<y<<")" << endl; }
    return this->P_Horiz[x + (this->getP_HX() * y)];
}
void Laby::setP_Horiz(int x, int y, porte val) {
    if (x < 0 || x >= this->getP_HX() || y < 0 || y >= this->getP_HY() ){ cout << "ERREUR4 : on essaye de modifier la porte horiz ("<<x<<","<<y<<")" << endl; }
    this->P_Horiz[x + (this->getP_HX() * y)] = val;
}


void Laby::debugHORIZ() {
    for (int y = 0; y < this->getP_HY(); y++ ) {    
        for (int x = 0; x < this->getP_HX(); x++ ) {
            cout << this->getP_Horiz(x,y) << " ";
        }
        cout << endl;
    }   
}
void Laby::debugVERTI() {
    for (int y = 0; y < this->getP_VY(); y++ ) {
        for (int x = 0; x < this->getP_VX(); x++ ) {
            cout << this->getP_Verti(x,y) << " ";
        }
        cout << endl;
    }   
}


string Laby::toTxt(string centre , bool coord , bool basic) {

    centre = centre[0];


    string R;
    if (basic) {
        string r;
        if (coord) {
            R = "  X";
            for (int x=0; x < this->getLargeur() ; x++) {
                R += " " + to_string(x%10) +  "  ";
            }
            R += "\n";
            R += "Y +";
            r = "0 |";
        } else {
            R = "+";
            r = "|";
        }

        for (int x = 0 ; x < this->getLargeur()-1 ; x++)  {
            if (this->getP_Verti(x,0)) {
                R += "---+";
                r += " " + centre + " |";
            } else {
                R += "----";
                r += " " + centre + "  ";
            }
        }
        R += "---+\n";
        r += " "+ centre +" |\n";
        R += r;
        
        for (int y = 0; y < this->getHauteur()-1; y++) {
            string l1;
            string l2;
            if (coord) {
                if (this->getP_Horiz(0,y)) {
                    l1 = "  +";
                } else {
                    l1 = "  |";
                }
                l2 = to_string((y+1)%10) + " |";
            } else {
                if (this->getP_Horiz(0,y)) {
                    l1 = "+";
                } else {
                    l1 = "|";
                }
                l2 = "|";
            }
            for (int x=0; x < this->getLargeur()-1; x++) {
                string inter = "+";
                
                if (this->getP_Horiz(x,y)) {
                    l1 += "---"+inter;
                } else {
                    l1 += "   "+inter;
                }
                if (getP_Verti(x,y+1)) {
                    l2 += " "+centre+" |";
                } else {
                    l2 += " "+centre+"  ";
                }
            }
            if (this->getP_Horiz(this->getP_HX()-1,y)) {
                l1 += "---+";
            } else {
                l1 += "   |";
            }
            l2 += " "+centre+" |";

            R += l1 + "\n";
            R += l2 + "\n";
        }
        if (coord) {
            r = "  +";
        } else {
            r = "+";
        }
        
        for (int x=0; x < this->getLargeur()-1; x++) {
            if (getP_Verti(x,this->getP_VY()-1)) {
                r += "---+";
            } else {
                r += "----";
            }
        }
        r += "---+";
        R += r;
        return R;
    
    } 
}


int Laby::nbChoix(int x, int y) {
    if (x < 0) { return -1; }
    if (y < 0) { return -1; }
    if (x >= this->getLargeur()) { return -1; }
    if (y >= this->getHauteur()) { return -1; }

    int nbpos = 4;

    if (! this->up(x, y)) { nbpos -=1 ; }
    if (! this->down(x, y)) { nbpos -=1 ; }
    if (! this->left(x, y)) { nbpos -=1 ; }
    if (! this->right(x, y)) { nbpos -=1 ; }

    return nbpos;
}

void Laby::repartitionPortes(int * p0, int * p1, int * p2, int * p3, int * p4) {
    *p0 = 0;
    *p1 = 0;
    *p2 = 0;
    *p3 = 0;
    *p4 = 0;
    int n; 
    for (int y = 0; y < this->getHauteur(); y ++) {
        for (int x = 0; x < this->getLargeur(); x ++) {
            n = this->nbChoix(x,y);
            // cout << "(" << x << ", " << y << ") => " << n << endl;
            if (n == 0) { *p0 += 1; }
            if (n == 1) { *p1 += 1; }
            if (n == 2) { *p2 += 1; }
            if (n == 3) { *p3 += 1; }
            if (n == 4) { *p4 += 1; }
        }
    }
}

int Laby::lenPlusCourtChemin(int xS, int yS, int xE, int yE) {
    int CASES[this->getLargeur()][this->getHauteur()];
    for (int x = 0; x < this->getLargeur() ; x++ ) {
        for (int y = 0; y < this->getHauteur() ; y++ ) {
            CASES[x][y] = -1;
        }
    }

    int xa,ya;

    typedef struct pos { int x,y; } pos ;
    queue<pos> pile;
    pos p;
    int N = 1;
    CASES[xS][yS] = 0;
    p.x = xS;
    p.y = yS;
    pile.push(p);
    while (! pile.empty() && CASES[xE][yE] == -1 ) {
        p = pile.front();
        pile.pop();
        xa = p.x;
        ya = p.y;

        if (this->up(xa, ya)) {
            if (CASES[xa][ya-1] == -1) {
                CASES[xa][ya-1] = CASES[xa][ya] +1;
                p.x = xa;
                p.y = ya-1;
                pile.push(p);
            }
        }
        if (this->down(xa, ya)) {
            if (CASES[xa][ya+1] == -1) {
                CASES[xa][ya+1] = CASES[xa][ya] +1;
                p.x = xa;
                p.y = ya+1;
                pile.push(p);
            }
        }
        if (this->left(xa, ya)) {
            if (CASES[xa-1][ya] == -1) {
                CASES[xa-1][ya] = CASES[xa][ya] +1;
                p.x = xa-1;
                p.y = ya;
                pile.push(p);
            }
        }
        if (this->right(xa, ya)) {
            if (CASES[xa+1][ya] == -1) {
                CASES[xa+1][ya] = CASES[xa][ya] +1;
                p.x = xa+1;
                p.y = ya;
                pile.push(p);
            }
        }
    }

    return CASES[xE][yE];
}

bool Laby::up(int x, int y) {
    if (y <= 0) {return false;}
    else { if (this->getP_Horiz(x,y-1)) {return false;} }
    return true;
}
bool Laby::down(int x, int y) {
    if (y >= this->getHauteur() -1) {return false;}
    else { if (this->getP_Horiz(x,y)) {return false;} }
    return true;
}
bool Laby::left(int x, int y) {
    if (x <= 0) {return false;}
    else { if (this->getP_Verti(x-1,y)) {return false;} }
    return true;
}
bool Laby::right(int x, int y) {
    if (x >= this->getLargeur() -1) {return false;}
    else { if (this->getP_Verti(x,y)) {return false;} }
    return true;
}

int Laby::nbComposanteConnexe() {
    int N = 0;
    int nbCaseOk = this->getLargeur() * this->getHauteur() ;
    bool CASES[this->getLargeur()][this->getHauteur()];
    for (int x = 0; x < this->getLargeur() ; x++ ) {
        for (int y = 0; y < this->getHauteur() ; y++ ) {
            CASES[x][y] = 0;
        }
    }

    typedef struct pos { int x,y; } pos ;
    stack<pos> pile;
    pos p;

    int xa,ya;
    while (nbCaseOk > 0) {
        for (int x = 0; x < this->getLargeur() ; x++ ) {
            for (int y = 0; y < this->getHauteur() ; y++ ) {
                if (CASES[x][y] == 0) {
                    xa = x;
                    ya = y;
                    break;
                    break;
                }
            }
        }

        N += 1;

        p.x = xa;
        p.y = ya;
        pile.push(p);
        while (! pile.empty()) {
            p = pile.top();
            pile.pop();
            xa = p.x;
            ya = p.y;
            if (CASES[xa][ya] == 0) {
                CASES[xa][ya] = 1;
                nbCaseOk -= 1;

                if (this->up(xa, ya)) {
                    if (CASES[xa][ya-1] == 0) {
                        p.x = xa;
                        p.y = ya-1;
                        pile.push(p);
                    }
                }
                if (this->down(xa, ya)) {
                    if (CASES[xa][ya+1] == 0) {
                        p.x = xa;
                        p.y = ya+1;
                        pile.push(p);
                    }
                }
                if (this->left(xa, ya)) {
                    if (CASES[xa-1][ya] == 0) {
                        p.x = xa-1;
                        p.y = ya;
                        pile.push(p);
                    }
                }
                if (this->right(xa, ya)) {
                    if (CASES[xa+1][ya] == 0) {
                        p.x = xa+1;
                        p.y = ya;
                        pile.push(p);
                    }
                }
            }
        }

    }

    return N;
}

int Laby::nbPortesFerm() {

    int S = 0;

    for (int y = 0; y < this->getP_HY(); y++ ) {
        for (int x = 0; x < this->getP_HX(); x++ ) {
            S += this->getP_Horiz(x,y);
        }
    }

    for (int y = 0; y < this->getP_VY(); y++ ) {
        for (int x = 0; x < this->getP_VX(); x++ ) {
            S += this->getP_Verti(x,y);
        }
    }

    return S;
}


int Laby::nbPortesOuv() {
    return this->nbPortes() - this->nbPortesFerm();

}
int Laby::nbPortes() {
    return this->getP_VX() * this->getP_VY() + this->getP_HX() * this->getP_HY();

}

int Laby::nbCycles() {
    return ((this->getLargeur() -1) * (this->getHauteur() -1)) - this->nbPortesFerm();
}

int Laby::plusLongChemin() {
    int CASES[this->getLargeur()][this->getHauteur()];
    for (int x = 0; x < this->getLargeur() ; x++ ) {
        for (int y = 0; y < this->getHauteur() ; y++ ) {
            CASES[x][y] = -1;
        }
    }

    int xa,ya;

    typedef struct pos { int x,y; } pos ;
    queue<pos> pile;
    pos p;
    int N = 1;

    CASES[0][0] = 0;
    p.x = 0;
    p.y = 0;
    pile.push(p);
    while (! pile.empty()) {
        p = pile.front();
        pile.pop();
        xa = p.x;
        ya = p.y;

        if (this->up(xa, ya)) {
            if (CASES[xa][ya-1] == -1) {
                CASES[xa][ya-1] = CASES[xa][ya] +1;
                p.x = xa;
                p.y = ya-1;
                pile.push(p);
            }
        }
        if (this->down(xa, ya)) {
            if (CASES[xa][ya+1] == -1) {
                CASES[xa][ya+1] = CASES[xa][ya] +1;
                p.x = xa;
                p.y = ya+1;
                pile.push(p);
            }
        }
        if (this->left(xa, ya)) {
            if (CASES[xa-1][ya] == -1) {
                CASES[xa-1][ya] = CASES[xa][ya] +1;
                p.x = xa-1;
                p.y = ya;
                pile.push(p);
            }
        }
        if (this->right(xa, ya)) {
            if (CASES[xa+1][ya] == -1) {
                CASES[xa+1][ya] = CASES[xa][ya] +1;
                p.x = xa+1;
                p.y = ya;
                pile.push(p);
            }
        }
    }

    for (int x = 0; x < this->getLargeur() ; x++ ) {
        for (int y = 0; y < this->getHauteur() ; y++ ) {
            CASES[x][y] = -1;
        }
    }



    CASES[xa][ya] = 0;
    N = 1;
    p.x = xa;
    p.y = ya;
    pile.push(p);

    while (! pile.empty()) {
        p = pile.front();
        pile.pop();
        xa = p.x;
        ya = p.y;

        if (this->up(xa, ya)) {
            if (CASES[xa][ya-1] == -1) {
                CASES[xa][ya-1] = CASES[xa][ya] +1;
                p.x = xa;
                p.y = ya-1;
                pile.push(p);
            }
        }
        if (this->down(xa, ya)) {
            if (CASES[xa][ya+1] == -1) {
                CASES[xa][ya+1] = CASES[xa][ya] +1;
                p.x = xa;
                p.y = ya+1;
                pile.push(p);
            }
        }
        if (this->left(xa, ya)) {
            if (CASES[xa-1][ya] == -1) {
                CASES[xa-1][ya] = CASES[xa][ya] +1;
                p.x = xa-1;
                p.y = ya;
                pile.push(p);
            }
        }
        if (this->right(xa, ya)) {
            if (CASES[xa+1][ya] == -1) {
                CASES[xa+1][ya] = CASES[xa][ya] +1;
                p.x = xa+1;
                p.y = ya;
                pile.push(p);
            }
        }
    }

    return CASES[xa][ya];
}




void Laby::genere_naif() {
    float p_ouv = 0.5;

    for (int y = 0; y < this->getP_HY(); y++ ) {
        for (int x = 0; x < this->getP_HX(); x++ ) {
            if (randomFloat() < p_ouv) {
                this->setP_Horiz(x,y,0);
            } else {
                this->setP_Horiz(x,y,1);
            }
        }
    }
    for (int y = 0; y < this->getP_VY(); y++ ) {
        for (int x = 0; x < this->getP_VX(); x++ ) {
            if (randomFloat() < p_ouv) {
                this->setP_Verti(x,y,0);
            } else {
                this->setP_Verti(x,y,1);
            }
        }
    }
}

void Laby::genere_empty() {
    // affectation des portes Horizontales
    for (int y = 0; y < this->getP_HY(); y++ ) {
        for (int x = 0; x < this->getP_HX(); x++ ) {
            this->setP_Horiz(x,y,0);
        }
    }
    // affectation des portes Verticales
    for (int y = 0; y < this->getP_VY(); y++ ) {
        for (int x = 0; x < this->getP_VX(); x++ ) {
            this->setP_Verti(x,y,0);
        }
    }
}

void Laby::genere_full() {
    // affectation des portes Horizontales
    for (int y = 0; y < this->getP_HY(); y++ ) {
        for (int x = 0; x < this->getP_HX(); x++ ) {
            this->setP_Horiz(x,y,1);
        }
    }
    // affectation des portes Verticales
    for (int y = 0; y < this->getP_VY(); y++ ) {
        for (int x = 0; x < this->getP_VX(); x++ ) {
            this->setP_Verti(x,y,1);
        }
    }
}

#include "algo_kruskal.cpp"

#include "algo_RBT.cpp"

#include "algo_RCD.cpp"

void Laby::ouvrePorte(int n) {
    int nb_porte_ferme = this->nbPortesFerm();
    if (nb_porte_ferme == 0) {
        return;
    } else if (nb_porte_ferme <= n) {
        this->genere_empty();
    } else {
        bool is_verti = false;
        int x = 0;
        int y = 0; 
        int n_restant = n;
        while (n_restant > 0) {
            // ouverture ?
            if (is_verti) {
                if (this->getP_Horiz(x,y) == 1) {
                    if ( randomRange(0, nb_porte_ferme) <= n ) {
                        this->setP_Horiz(x,y,0);
                        n_restant --;
                    }
                }
            } else {
                if (this->getP_Verti(x,y) == 1) {
                    if ( randomRange(0, nb_porte_ferme) <= n ) {
                        this->setP_Verti(x,y,0);
                        n_restant --;
                    }
                }
            }

            // nouvelle porte
            if (is_verti) {
                if (x+1 < this->getP_HX()) {
                    x += 1;
                } else {
                    x = 0;
                    if (y+1 < this->getP_HY()) {
                        y += 1;
                    } else {
                        y = 0;
                        is_verti = false;
                    }
                }
            } else {
                if (x+1 < this->getP_VX()) {
                    x += 1;
                } else {
                    x = 0;
                    if (y+1 < this->getP_VY()) {
                        y += 1;
                    } else {
                        y = 0;
                        is_verti = true;
                    }
                }
            }


        }
    }
}




void Laby::resout(Resolution * reso, int xS, int yS, int xE, int yE, string heuri) {
    transform(heuri.begin(), heuri.end(), heuri.begin(), ::tolower);

    if ((heuri == "dijkstra") || (heuri == "d")) {
        this->resout_dijkstra(reso, xS, yS, xE, yE);
    }
    if ((heuri == "human1") || (heuri == "h1")) {
        this->resout_human1(reso, xS, yS, xE, yE);
    }
    if ((heuri == "human2") || (heuri == "h2")) {
        this->resout_human2(reso, xS, yS, xE, yE);
    }
    if ((heuri == "a*") || (heuri == "aetoile") || (heuri == "a_etoile")) {
        this->resout_Aetoile(reso, xS, yS, xE, yE);
    }
    if ((heuri == "a*b") || (heuri == "aetoilebis") || (heuri == "a_etoilebis")) {
        this->resout_Aetoilebis(reso, xS, yS, xE, yE);
    }
    if ((heuri == "a*o") || (heuri == "aetoileok") || (heuri == "a_etoileok")) {
        this->resout_AetoileOk(reso, xS, yS, xE, yE);
    }
}


#include "resol_dijkstra.cpp"

#include "resol_humain1.cpp"
#include "resol_humain2.cpp"

#include "resol_AEtoile.cpp"
#include "resol_AEtoilebis.cpp"
#include "resol_AEtoileOk.cpp"

