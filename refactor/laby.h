
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <random>
#include <stack>
#include <queue>
#include <algorithm>
#include <chrono>

using namespace std;

float randomFloat();
int randomRange(int mini, int maxi);
int dManha(int x1, int y1, int x2, int y2);
typedef bool porte;

typedef struct {
    int lenChe;
    int nbExplore;
    double time;
    bool possible;
} Resolution ;






// Representation of maze :
// A labyrinth is a set of 2 door panels.
// For a maze 5*5 :
// Theory :                        // Example :
//    X ->
//  Y a b c d  / vertical doors     //      1 1 1 1  / vertical doors
//  | e f g h                       //      1 0 1 0
//  v i j k l                       //      0 1 0 1
//    m n o p                       //      0 0 0 0
//    q r s t                       //      0 1 0 0
// 
//    X ->
//  Y A B C D E / horizontal doors  //      0 0 0 0 0 / horizontal doors
//  | F G H I J                     //      0 0 0 0 1
//  v K L M N O                     //      0 1 1 1 0
//    P Q R S T                     //      0 1 0 1 1
//                                            //
// ==>>                                       // ==>>
//   X 0   1   2   3   4                      //   X 0   1   2   3   4
// Y ┌───┬───┬───┬───┬───┐                    // Y ┌───┬───┬───┬───┬───┐
// 0 │ ∙ a ∙ b ∙ c ∙ d ∙ │                    // 0 │ ∙ │ ∙ │ ∙ │ ∙ │ ∙ │
//   ├─A─┼─B─┼─C─┼─D─┼─E─┤                    //   │   │   │   │   │   │
// 1 │ ∙ e ∙ f ∙ g ∙ h ∙ │                    // 1 │ ∙ │ ∙   ∙ │ ∙   ∙ │
//   ├─F─┼─G─┼─H─┼─I─┼─J─┤                    //   │   │   │   │   ┌───┤
// 2 │ ∙ i ∙ j ∙ k ∙ l ∙ │                    // 2 │ ∙   ∙ │ ∙   ∙ │ ∙ │
//   ├─K─┼─L─┼─M─┼─N─┼─O─┤                    //   │   ╶───┴───────┘   │
// 3 │ ∙ m ∙ n ∙ o ∙ p ∙ │                    // 3 │ ∙   ∙   ∙   ∙   ∙ │
//   ├─P─┼─Q─┼─R─┼─S─┼─T─┤                    //   │   ╶───┐   ╶───────│
// 4 │ ∙ q ∙ r ∙ s ∙ t ∙ │                    // 4 │ ∙   ∙ │ ∙   ∙   ∙ │
//   └───┴───┴───┴───┴───┘                    //   └───────┴───────────┘

class Laby {
    public:
        Laby(int Largeur, int Hauteur, string name_algo="naif");
        ~Laby();
        // naif / n                         => algo naif
        // empty / e                        => aucune porte
        // full / f                         => toutes les portes
        // kruskal / k                      => algo de kruskal
        // recursive_backtrack / rbt        => algo de recursive backtrack
        // recursive_chamber_division / rcd => algo de division par chambre recursive
        
        int getLargeur();
        int getHauteur();
    
        int getP_HY();
        int getP_HX();
        void setP_Horiz(int x, int y, porte val);
        porte  getP_Horiz(int x, int y);

        int getP_VY();
        int getP_VX();
        void setP_Verti(int x, int y, porte val);
        porte  getP_Verti(int x, int y);
    
        void ouvrePorte(int n);

        string toTxt(string centre = " ", bool coord = true, bool basic = true);
        
        void debugHORIZ();
        void debugVERTI();

        int nbChoix(int x, int y);
        void repartitionPortes(int * p0, int * p1, int * p2, int * p3, int * p4);

        int lenPlusCourtChemin(int xS, int yS, int xE, int yE);
        int plusLongChemin();

        int nbComposanteConnexe();
        int nbPortesFerm();
        int nbPortesOuv();
        int nbPortes();
        int nbCycles();

        void resout(Resolution * reso, int xS, int yS, int xE, int yE, string heuri="");

        bool up(int x, int y);
        bool down(int x, int y);
        bool left(int x, int y);
        bool right(int x, int y);

    private:
        void genere_naif();
        void genere_empty();
        void genere_full();
        void genere_kruskal();
        void genere_RBT();
        void genere_RCD();

        void resout_dijkstra(Resolution * reso, int xS, int yS, int xE, int yE);
        void resout_human1(Resolution * reso, int xS, int yS, int xE, int yE);
        void resout_human2(Resolution * reso, int xS, int yS, int xE, int yE);
        void resout_Aetoile(Resolution * reso, int xS, int yS, int xE, int yE);
        void resout_Aetoilebis(Resolution * reso, int xS, int yS, int xE, int yE);
        void resout_AetoileOk(Resolution * reso, int xS, int yS, int xE, int yE);

        int Largeur;  // Taille du labyrinthe
        int Hauteur;

        int P_HY; // taille du tableau de portes horizontal
        int P_HX;
    
        int P_VY; // taille du tableau de portes vertical
        int P_VX;
        
        porte* P_Horiz; // tableau à 2 dimensions (les portes horizontales)
        porte* P_Verti; // tableau à 2 dimensions (les portes verticales)
};
