#include <stack>
#include <set>




int GetSample1(set<int> &s ) {
  double r = rand() % s.size();
  std::set<int>::iterator it = s.begin();
  for (; r != 0; r--) it++;
  return *it;
}
void Laby::resout_human1(Resolution * reso, int xS, int yS, int xE, int yE) {

    int nbStep = 0;

    int VISION = 30; // Portée de la vision

    int CONDAMNE[this->getLargeur()][this->getHauteur()];
    for (int x = 0; x < this->getLargeur(); x++ ) {
        for (int y = 0; y < this->getHauteur(); y++ ) {
            CONDAMNE[x][y] = 0;
        }
    }

    typedef struct pos { int x,y; } pos ;
    stack<pos> pile;

    auto start = chrono::high_resolution_clock::now();
    
    pos p;
    p.x = xS;
    p.y = yS;
    pile.push(p);


    while ( (! pile.empty()) && !((xS == xE) && (yS == yE))) {
        // AVANCE
        nbStep ++;



        int VU_DIR = -1;
        
        int Pup    = 0; // nbr de porte que l'on 'voit' en haut (Y-)
        if (this->up(xS,yS) && CONDAMNE[xS][yS-1] == 0) {
            bool end = true;
            for (int i = 1; i < VISION ; i++) {
                if (this->left(xS,yS-i)) { Pup ++; }
                if (this->right(xS,yS-i)) { Pup ++; }
                if ((xS == xE) && (yS-i == yE)) {VU_DIR = 0;}
                if ( !(this->up(xS,yS-i)) ) { end = false; break; }
            }
            if (end) { if (this->up(xS,yS-(VISION))) {Pup ++;} }
        }

        int Pdown  = 0; // nbr de porte que l'on 'voit' en bas  (Y+)
        if (this->down(xS,yS) && CONDAMNE[xS][yS+1] == 0) {
            bool end = true;
            for (int i = 1; i < VISION ; i++) {
                if (this->left(xS,yS+i)) { Pdown ++; }
                if (this->right(xS,yS+i)) { Pdown ++; }
                if ((xS == xE) && (yS+i == yE)) {VU_DIR = 2;}
                if ( !(this->down(xS,yS+i)) ) { end = false; break; }
            }
            if (end) { if (this->down(xS,yS+(VISION))) {Pdown ++;} }
        }

        int Pleft  = 0; // nbr de porte que l'on 'voit' en gauche (X-)
        if (this->left(xS,yS) && CONDAMNE[xS-1][yS] == 0) {
            bool end = true;
            for (int i = 1; i < VISION ; i++) {
                if (this->up(xS-i,yS)) { Pleft ++; }
                if (this->down(xS-i,yS)) { Pleft ++; }
                if ((xS-i == xE) && (yS == yE)) {VU_DIR = 3;}
                if ( !(this->left(xS-i,yS)) ) { end = false; break; }
            }
            if (end) { if (this->left(xS-(VISION),yS)) {Pleft ++;} }
        }

        int Pright = 0; // nbr de porte que l'on 'voit' en droite (X+)
        if (this->right(xS,yS) && CONDAMNE[xS+1][yS] == 0) {
            bool end = true;
            for (int i = 1; i < VISION ; i++) {
                if (this->up(xS+i,yS)) { Pright ++; }
                if (this->down(xS+i,yS)) { Pright ++; }
                if ((xS+i == xE) && (yS == yE)) {VU_DIR = 1;}
                if ( !(this->right(xS+i,yS)) ) { end = false; break; }
            }
            if (end) { if (this->right(xS+(VISION),yS)) {Pright ++;} }
        }

        if (VU_DIR == -1) { // Cas où l'on n'a pas vu la sortie,
            if (Pup == 0 && Pright == 0 && Pleft == 0 && Pdown == 0) {
                // la case (xS,yS) ne mene à rien : demi-tour
                pile.pop();
                CONDAMNE[xS][yS] = 2;
                if (! pile.empty() ){
                    pos p;
                    p = pile.top();
                    xS = p.x;
                    yS = p.y;
                }
            } else {

                int vMax1 = -1;
                if (Pup    > vMax1) {vMax1 = Pup    ; }
                if (Pright > vMax1) {vMax1 = Pright ; }
                if (Pdown  > vMax1) {vMax1 = Pdown  ; }
                if (Pleft  > vMax1) {vMax1 = Pleft  ; }

                set<int> setOfChoice1;
                if (Pup    >= vMax1) { setOfChoice1.insert(0); }
                if (Pright >= vMax1) { setOfChoice1.insert(1); }
                if (Pdown  >= vMax1) { setOfChoice1.insert(2); }
                if (Pleft  >= vMax1) { setOfChoice1.insert(3); }

                int vMax2 = dManha(xS,yS,xE,yE) *2 ;
                for (set<int>::iterator it=setOfChoice1.begin(); it!=setOfChoice1.end(); ++it) {
                    int i = *it;
                    if ( i == 0 && dManha(xS,yS-1,xE,yE) < vMax2) {vMax2 = dManha(xS,yS-1,xE,yE);}
                    if ( i == 1 && dManha(xS+1,yS,xE,yE) < vMax2) {vMax2 = dManha(xS+1,yS,xE,yE);}
                    if ( i == 2 && dManha(xS,yS+1,xE,yE) < vMax2) {vMax2 = dManha(xS,yS+1,xE,yE);}
                    if ( i == 3 && dManha(xS-1,yS,xE,yE) < vMax2) {vMax2 = dManha(xS-1,yS,xE,yE);}
                }

                set<int> setOfChoice2;
                for (set<int>::iterator it=setOfChoice1.begin(); it!=setOfChoice1.end(); ++it) {
                    int i = *it;
                    if ( i == 0 && dManha(xS,yS-1,xE,yE) == vMax2) {setOfChoice2.insert(0); }
                    if ( i == 1 && dManha(xS+1,yS,xE,yE) == vMax2) {setOfChoice2.insert(1); }
                    if ( i == 2 && dManha(xS,yS+1,xE,yE) == vMax2) {setOfChoice2.insert(2); }
                    if ( i == 3 && dManha(xS-1,yS,xE,yE) == vMax2) {setOfChoice2.insert(3); }
                }
                
                int CHOIX = GetSample1( setOfChoice2 );
                CONDAMNE[xS][yS] = 1;
                if ( CHOIX == 0 ) { xS = xS   ; yS = yS-1 ; }
                if ( CHOIX == 1 ) { xS = xS+1 ; yS = yS   ; }
                if ( CHOIX == 2 ) { xS = xS   ; yS = yS+1 ; }
                if ( CHOIX == 3 ) { xS = xS-1 ; yS = yS   ; }
                pos p;
                p.x = xS;
                p.y = yS;
                pile.push(p);
            }

        } else if (VU_DIR == 0) {
            yS --;
            while (! ((xS == xE) && (yS == yE))) {
                pos p;
                p.x = xS;
                p.y = yS;
                pile.push(p);
                nbStep ++; yS --;
            }
        } else if (VU_DIR == 1) {
            xS ++;
            while (! ((xS == xE) && (yS == yE))) {
                pos p;
                p.x = xS;
                p.y = yS;
                pile.push(p);
                nbStep ++; xS ++;
            }            
        } else if (VU_DIR == 2) {
            yS ++;
            while (! ((xS == xE) && (yS == yE))) {
                pos p;
                p.x = xS;
                p.y = yS;
                pile.push(p);
                nbStep ++; yS ++;
            }            
        } else if (VU_DIR == 3) {
            xS --;
            while (! ((xS == xE) && (yS == yE))) {
                pos p;
                p.x = xS;
                p.y = yS;
                pile.push(p);
                nbStep ++; xS --;
            }            
        }
    }


    auto end = chrono::high_resolution_clock::now();
    chrono::duration<double, milli> float_ms = end - start;
    if (pile.empty()) {reso->possible = false; }
    else { reso->possible = true; }
    reso->lenChe = pile.size();
    reso->nbExplore = nbStep;
    reso->time = float_ms.count();

}