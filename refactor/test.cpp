#include "laby.h"

int main(int argc, char* argv[]) {
    srand(time(NULL));
    // srand(5);


    int X = 20;
    int Y = 10;

	int x1 = 0;
	int y1 = 0;

	int x2 = X-1;
	int y2 = Y-1;
	int nbPO = 20;


    // for (int N = 0; N < 5; N++) {
		Laby *myLaby = new Laby(X,Y,"k");

		myLaby->ouvrePorte(10);
		// myLaby->setP_Verti(4,0,0);
		// myLaby->setP_Horiz(4,1,0);
		// myLaby->setP_Horiz(4,2,1);
		// myLaby->setP_Verti(4,2,0);
		// myLaby->setP_Verti(4,1,1);
		// myLaby->setP_Verti(5,1,0);
		// myLaby->setP_Verti(5,2,1);
		// myLaby->setP_Verti(6,1,1);
		// myLaby->setP_Verti(6,2,0);
		// myLaby->setP_Horiz(7,1,0);
		// myLaby->setP_Horiz(7,2,1);
		// myLaby->setP_Verti(8,1,0);
		// myLaby->setP_Verti(8,2,1);
		cout << myLaby->toTxt() <<endl;

		Resolution myResol;
		myLaby->resout(& myResol,x1,y1,x2,y2,"d");

		cout << endl;
		cout << "Stats de la résolution dijkstra :" << endl; // temps, pas
		cout << " Trajet de ("<<x1<<","<<y1<<") vers (" << x2 << "," << y2 << ")"<< endl;
		if (myResol.possible) {
			cout << " Les cases sont reliables." << endl;
			cout << " Longueur du chemin trouvé = " << myResol.lenChe << endl; 
		} else {
			cout << " Les cases ne sont pas reliables." << endl;
		}
		cout << " Nbr d'etape   = " << myResol.nbExplore << endl;
		cout << " Temps (ms)    = " << myResol.time << endl;

		myLaby->resout(& myResol,x1,y1,x2,y2,"a*b");

		cout << endl;
		cout << "Stats de la résolution a*b :" << endl; // temps, pas
		cout << " Trajet de ("<<x1<<","<<y1<<") vers (" << x2 << "," << y2 << ")"<< endl;
		if (myResol.possible) {
			cout << " Les cases sont reliables." << endl;
			cout << " Longueur du chemin trouvé = " << myResol.lenChe << endl; 
		} else {
			cout << " Les cases ne sont pas reliables." << endl;
		}
		cout << " Nbr d'etape   = " << myResol.nbExplore << endl;
		cout << " Temps (ms)    = " << myResol.time << endl;



		myLaby->resout(& myResol,x1,y1,x2,y2,"a*o");

		cout << endl;
		cout << "Stats de la résolution a*o :" << endl; // temps, pas
		cout << " Trajet de ("<<x1<<","<<y1<<") vers (" << x2 << "," << y2 << ")"<< endl;
		if (myResol.possible) {
			cout << " Les cases sont reliables." << endl;
			cout << " Longueur du chemin trouvé = " << myResol.lenChe << endl; 
		} else {
			cout << " Les cases ne sont pas reliables." << endl;
		}
		cout << " Nbr d'etape   = " << myResol.nbExplore << endl;
		cout << " Temps (ms)    = " << myResol.time << endl;
	// }
	return 0;
}
