#include "laby.h"
#include <iostream>
#include <fstream>


/************************************************************/
/**                  PARAMETRE EXPERIENCE                  **/
/************************************************************/

#define NB_SIZE 10
int size[NB_SIZE] = {50,100,150,200,250,300,350,400,450,500};

#define NB_OUV_POR 11
int ouv_por[NB_OUV_POR] = {0,20,40,60,80,100,120,140,160,180,200};

#define NB_ALGO_GEN 3
const char* algos[NB_ALGO_GEN] = {"rbt", "rcd", "k"};

#define ECHANTILLON 2
double seed = time(NULL);
// double seed = 1.6198e+09;

/************************************************************/
/************************************************************/
/************************************************************/



int main() {
    srand(seed);

    ofstream myfile;
    ofstream myfileinfo;
    myfile.open ("brut.txt",ios::app);
    myfileinfo.open ("info.txt",ios::app);
    myfile << "id;taille;cycle;algo;temps;" ; 
    myfile << "d1;d2;dia;p0;p1;p2;p3;p4;";
    myfile << endl;
    myfile.close();

    myfileinfo << "seed : " << seed << endl;

    cout << "Il y aura " << NB_SIZE << " tailles differentes." << endl;
    cout << "Il y aura " << NB_OUV_POR << " ouvertures de portes differentes." << endl;
    cout << "Il y aura " << NB_ALGO_GEN << " algos de generations." << endl;
    cout << "Il y aura " << ECHANTILLON << " labyrinthes par echantillons." << endl;
    int NB = NB_SIZE * NB_OUV_POR * NB_ALGO_GEN * ECHANTILLON;
    int cpt = 0;
    cout << "Ce qui fait un total de " << NB << " labyrinthes géneres." << endl << endl;

    myfileinfo << "Il y aura " << NB_SIZE << " tailles differentes." << endl;
    myfileinfo << "Il y aura " << NB_OUV_POR << " ouvertures de portes differentes." << endl;
    myfileinfo << "Il y aura " << NB_ALGO_GEN << " algos de generations." << endl;
    myfileinfo << "Il y aura " << ECHANTILLON << " labyrinthes par echantillons." << endl;
    myfileinfo << "Ce qui fait un total de " << NB << " labyrinthes géneres." << endl << endl;

    myfileinfo.close();
    double temps_total = time(NULL);

    for (int i_s = 0; i_s < NB_SIZE ; i_s ++) {
        int s = size[i_s];
        for (int i_o = 0; i_o < NB_OUV_POR ; i_o ++) {
            int o = ouv_por[i_o];
            for (int i_a = 0; i_a < NB_ALGO_GEN ; i_a ++) {
                const char* algo = algos[i_a];
                for (int i = 0 ; i < ECHANTILLON; i ++) {

                    cout << cpt << " -> labyrinthe " << i << " de la serie ("<<s<<","<<s<<") avec l'algo "<<algo<<" et "<<o<<" cycles."<<endl;
                    



                    auto start = std::chrono::high_resolution_clock::now();
                
                    // GENERATION
                    Laby *myLaby = new Laby(s,s,algo);
                    if (o != 0) { myLaby->ouvrePorte(o); }
                
                    auto end = std::chrono::high_resolution_clock::now();
                    std::chrono::duration<double, std::milli> float_ms = end - start;

                    cout << "Generation ok. ";
                    // CARACTERISATION
                    int d1  = myLaby->lenPlusCourtChemin(0,0,s-1,s-1);
                    int d2  = myLaby->lenPlusCourtChemin(0,s-1,s-1,0);
                    int dia = myLaby->plusLongChemin();
                    int p0 = 0; int p1 = 0; int p2 = 0; int p3 = 0; int p4 = 0;
                    myLaby->repartitionPortes(& p0,& p1,& p2,& p3,& p4);

                    cout << "Caracterisation ok. ";
                    // ENREGISTREMENT

                    ofstream myfile;
                    myfile.open ("brut.txt",ios::app);

                    myfile << cpt << ";" << s << ";" << o << ";" << algo << ";" << float_ms.count() << ";"; 
                    myfile << d1 << ";" << d2 << ";" << dia << ";" << p0 << ";" << p1 << ";" << p2 << ";" << p3 << ";" << p4 ;

                    myfile << endl;
                    myfile.close();
                    
                    cout << "Enregistrement ok. ";
                    cout << endl;
                    cpt ++;
                    delete myLaby;
                }
            }
        }
    }
    temps_total = time(NULL) - temps_total ;


    cout << "Done !" << endl;
    cout << cpt << "/" << NB << endl;

    myfileinfo << "Done !" << endl;
    myfileinfo << "total time (s) : " << temps_total << endl;
    myfileinfo << cpt << "/" << NB << endl;

	return 0;
}