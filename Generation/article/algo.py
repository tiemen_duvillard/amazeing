from random import random,randrange
import os

# nombre de noeud
n = 1000



Next = [None] * n
InTree = [False] * n

def Chance(epsi):
    return random() <= epsi

def randomSuccessor(u):
    r = randrange(n-1)
    if r >= u: return r+1
    else     : return r

def randomTree():
    epsi = 0.5
    tree = attempt(epsi)
    while tree == "Failure":
        epsi = epsi/2
        tree = attempt(epsi)
    return tree

def attempt(epsi):
    for i in range(n):
        InTree[i] = False
    num_roots = 0

    for i in range(n):
        u = i
        while not InTree[u]:
            if Chance(epsi):
                Next[u] = None
                InTree[u] = True
                num_roots += 1
                if num_roots == 2 :
                    return "Failure"
            else:
                Next[u] = randomSuccessor(u)
                u = Next[u]
        u = i
        while not InTree[u] :
            InTree[u] = True
            u = Next[u]
    return Next




# generation d'un .dot et d'une image png
def affiche(filename="test"):
    file_dot = open(filename+".dot","w")
    file_dot.write("digraph {\n")
    REL = ""
    for i in range(n):
        if Next[i] != None:
            file_dot.write("  {} [label = \"{}\", regular=1, fixedsize =True, height=0.5 ]\n".format(i,i))
            REL += " {} -> {} \n".format(i, Next[i])
        else:
            file_dot.write("  {} [label = \"{}\",style=filled,fillcolor=grey, regular=1, height=0.3]\n".format(i,i))

    file_dot.write(REL)
    file_dot.write("}")
    file_dot.close()

    os.system("dot -Tpng {} > {}".format(filename+".dot",filename+".png"))



### Main ###

randomTree()
affiche()

